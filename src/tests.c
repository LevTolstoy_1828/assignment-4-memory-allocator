#include "tests.h"

void test1(){
    printf("TEST1: successful allocation.\n");
    void *heap = heap_init(100);
    void* alloc = _malloc(10);

    if (heap) {
        printf("heap initiated:\n");
        debug_heap(stdout, heap);
    }

    if (!heap) {
        fprintf(stderr, "%s", "failed");
        return;
    }

    if(!alloc){
        printf("Failed in malloc\n");
        return;
    }
    _free(alloc);

    munmap(HEAP_START, 8192);

    printf("TEST1 passed\n\n");
}
void test2(){
    printf("TEST2: free one of many blocks\n");

    void *heap = heap_init(100);

    if(!heap){
        printf("FAILED: heap didn't initiate\n");
        return;
    }

    void* alloc_1 = _malloc(10);
    void* alloc_2 = _malloc(20);
    void* alloc_3 = _malloc(30);

    if(!alloc_1 || !alloc_2 || !alloc_3){
        printf("Failed in malloc\n");
        return;
    }

    debug_heap(stdout, heap);
    _free(alloc_3);
    debug_heap(stdout, heap);
    _free(alloc_1);
    _free(alloc_2);
    munmap(HEAP_START, 8192);
    printf("TEST2 passed\n\n");
}

void test3(){
    printf("TEST3: free several of many blocks\n");

    void* heap = heap_init(100);

    if(!heap){
        printf("FAILED: heap didn't initiate\n");
        return;
    }

    void* alloc_1 = _malloc(10);
    void* alloc_2 = _malloc(20);
    void* alloc_3 = _malloc(30);
    void* alloc_4 = _malloc(40);
    void* alloc_5 = _malloc(50);

    if(!alloc_1 || !alloc_2 || !alloc_3 || !alloc_4 || !alloc_5){
        printf("Failed in malloc\n");
        return;
    }

    debug_heap(stdout, heap);
    _free(alloc_1);
    _free(alloc_2);
    debug_heap(stdout, heap);
    _free(alloc_3);
    _free(alloc_4);
    _free(alloc_5);

    munmap(HEAP_START, 8192);
    printf("TEST3 passed\n\n");
}

void test4(){
    printf("TEST4: trying to allocate block but there is no memory left in heap\n");

    void* heap = heap_init(100);

    if(!heap){
        printf("FAILED: heap didn't initiate\n");
        return;
    }

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10000);

    if(!alloc_1){
        printf("Failed in malloc\n");
        return;
    }

    debug_heap(stdout, heap);

    _free(alloc_1);

    munmap(heap, 20480);
    printf("TEST4 passed\n\n");

}

void test5(){
    printf("TEST5: trying to allocate block but there is no memory left in heap. Heap expends in other place\n");

    void* heap = heap_init(100);

    if(!heap){
        printf("FAILED: heap didn't initiate\n");
        return;
    }


    void* map = mmap(HEAP_START + 8192, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10000);

    if(!alloc_1){
        printf("Failed in malloc\n");
        return;
    }

    debug_heap(stdout, heap);

    _free(alloc_1);
    debug_heap(stdout, heap);

    munmap(map, 4096);

    printf("TEST5 passed\n\n");
}

void run_tests(){
    test1();
    test2();
    test3();
    test4();
    test5();
}