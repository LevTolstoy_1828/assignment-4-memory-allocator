#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H

#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"

#include <stdio.h>
#include <string.h>

void run_tests();


#endif
